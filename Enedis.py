import os;
import datetime;
import json;
import subprocess
from time import time
import pytz
from tb_device_mqtt import TBDeviceMqttClient, TBPublishInfo

# Use conso.vercel.app to generate new token

PATH = os.path.dirname(os.path.abspath(__file__)).replace("\\", "/")
PATH_CONSO_DATA = PATH + "/conso.json"
PATH_PROD_DATA = PATH + "/prod.json"
if os.name == 'nt':
    LINKY = "linky.exe"
else :
    LINKY = "linky"

def dateIsValid(date_text):
    try:
        datetime.datetime.strptime(date_text, '%Y-%m-%d')
        return True
    except ValueError:
        return False

def get_last_date():
    path = PATH + "/start_date.txt"
    date_origin = (datetime.date.today() - datetime.timedelta(days=7)).strftime("%Y-%m-%d")
    try:
        src = open(path,'r') 
    except Exception as e :
        print("Aucun fichier de date => récupération de toutes les données depuis le commencement")
        return(date_origin)

    date = src.readline().rstrip()
    if(dateIsValid(date) == False) :
        print("Mauvaise date => récupération de toutes les données depuis le commencement")
        src.close()
        return(date_origin)
    
    src.close()
    return(date)

def save_last_date(last_date) :
    path = PATH + "/start_date.txt"
    try:
        src = open(path,'w') 
    except Exception as e :
        print("Impossible de sauvegarder la date de fin")
        return
    
    src.write(last_date)
    src.close()

def get_config() :
    path = PATH + "/config.json"
    try:
        src = open(path, 'r') 
    except Exception as e :
        print("Aucun fichier de config : " + str(e))
        exit(-1)

    lines = src.read(os.path.getsize(path))
    src.close()
    return(json.loads(lines))

def enedis_auth(access_token, refresh_token, id) :
    try :
        retval = subprocess.check_call(PATH + "/" + LINKY + " auth -a '%s' -r '%s' -u '%s'"%(access_token, refresh_token, id),shell=True )
    except Exception as e :
        print("Impossible de se connecter a l'API enedis")
        exit(-1)

def enedis_get_data(type, start, id, output) :
    try :
        retval = subprocess.check_call(PATH + "/" + LINKY + " %s --start '%s' -u '%s' --output %s"%(type, start, id, output), shell=True )
    except Exception as e :
        print("Impossible de récupérer la data")

def parse_json_data() :
    try:
        src_conso = open(PATH_CONSO_DATA,'r') 
        src_prod = open(PATH_PROD_DATA,'r') 
    except Exception as e :
        print("Aucun fichier de donnee : " + str(e))
        return


    lines = src_conso.read(os.path.getsize(PATH_CONSO_DATA))
    data = json.loads(lines)
    tb_data = {}
    for d in data["data"] :
        tb_data[d["date"]] = {"consomation" : float(d["value"])}

    lines = src_prod.read(os.path.getsize(PATH_PROD_DATA))
    data = json.loads(lines)
    for d in data["data"] :
        tb_data[d["date"]]["production"] = float(d["value"])
    
    src_conso.close()
    src_prod.close()
    return(tb_data)

def tb_convert_data(data) :
    tb_data = []
    for date, value in data.items() :
        local_tz = pytz.timezone("Europe/Paris")
        local_dt = datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S")
        timestamp = int(local_tz.localize(local_dt).timestamp() * 1000)
        tb_data.append({"ts":timestamp, "values":value})
    
    return tb_data

def tb_send_data(broker, token, data) :
    client = TBDeviceMqttClient(broker, 1883, token)
    client.connect()
    for d in data :
        result = client.send_telemetry(d)
        success = result.get() == TBPublishInfo.TB_ERR_SUCCESS
    client.disconnect()
    return(success)

##################################################################
########################### START ################################
##################################################################

#definition des variables préliminaires
start_date = get_last_date()
config = get_config()



enedis_auth(config["access"], config["refresh"], config["production"])
enedis_auth(config["access"], config["refresh"], config["consomation"])

enedis_get_data("loadcurve", start_date, config["consomation"], PATH + "/conso.json")
enedis_get_data("loadcurveprod", start_date, config["production"], PATH + "/prod.json")

data = parse_json_data()
last_date = list(data.keys())[-1]
data = tb_convert_data(data)

if( tb_send_data(config["tb_broker"], config["tb_token"], data) == True):
    save_last_date(last_date[:10])
else :
    print("Impossible d'envoyer les données")



